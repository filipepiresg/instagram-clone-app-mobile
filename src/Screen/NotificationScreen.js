import React, { Component } from "react"
import { Text, View } from "react-native"

export default class NotificationScreen extends Component {
  static navigationOptions = {
    headerTitle: () => (
      <Text style={{ fontSize: 16, fontWeight: "bold" }}>Notification</Text>
    )
  }
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Text> Notification </Text>
      </View>
    )
  }
}
