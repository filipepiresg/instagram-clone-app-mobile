import React, { Component } from "react"
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
  KeyboardAvoidingView
} from "react-native"
import Icon from "react-native-vector-icons/Ionicons"

import { recoverPass } from "../bin/authService"

export default class ForgotPassScreen extends Component {
  static navigationOptions = {
    header: null
  }

  state = {
    username: "",
    email: "",
    isActiveButton: false
  }

  componentDidUpdate(_, prevState) {
    if (
      prevState.username !== this.state.username ||
      prevState.email !== this.state.email
    )
      this._handleButton()
  }

  _handleButton = () => {
    const { username, email } = this.state
    if (username && email) this.setState({ isActiveButton: true })
    else this.setState({ isActiveButton: false })
  }

  _cleanStates = (callback = Function) => {
    this.setState(
      { username: "", email: "", isActiveButton: false },
      () => callback
    )
  }

  _forgotPassAsync = async () => {
    const { username, email } = this.state
    await recoverPass(username, email)
      .then(data => {
        this._cleanStates(Alert.alert("Password", data.password))
      })
      .catch(err => {
        this._cleanStates(Alert.alert("Error", err))
      })
  }

  render() {
    const { username, email, isActiveButton } = this.state
    const { navigation } = this.props
    return (
      <KeyboardAvoidingView style={styles.container}>
        <View style={styles.containerLogo}>
          <Icon color="#000" name="ios-lock" size={60} />
          <Text
            style={[
              styles.textLogo,
              { fontSize: 24, fontWeight: "bold", marginVertical: 20 }
            ]}
          >
            Trouble logging in?
          </Text>
          <Text style={[styles.textLogo, { fontSize: 16 }]}>
            Enter your username and email and we'll send you a alert to get back
            into your password
          </Text>
        </View>
        <View style={styles.containerData}>
          <TextInput
            placeholder="Email"
            value={email}
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.textInput}
            onChangeText={email => this.setState({ email })}
          />
          <TextInput
            placeholder="Username"
            value={username}
            keyboardType="name-phone-pad"
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.textInput}
            onChangeText={username => this.setState({ username })}
          />
          <TouchableOpacity
            onPress={this._forgotPassAsync}
            disabled={!isActiveButton}
            style={[
              styles.button,
              {
                backgroundColor: isActiveButton
                  ? "rgb(70,100, 240)"
                  : "rgb(170,200, 240)"
              }
            ]}
          >
            <Text style={styles.textButtonLogin}> Recover Password</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.containerFooter}>
          <View style={{ flex: 3 }} />
          <View style={styles.footer}>
            <TouchableOpacity
              onPress={() => this._cleanStates(navigation.pop())}
            >
              <Text style={styles.textLink}>Back To Login</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: { flex: 1 },
  containerLogo: {
    flex: 2,
    justifyContent: "flex-end",
    alignItems: "center",
    paddingHorizontal: 40,
    paddingBottom: 20
  },
  containerData: { flex: 2, paddingHorizontal: 15 },
  containerFooter: { flex: 1 },
  button: {
    borderRadius: 5,
    paddingVertical: 15,
    marginVertical: 30
  },
  textLogo: {
    flexWrap: "wrap",
    textAlign: "center"
  },
  textButtonLogin: {
    textAlign: "center",
    color: "white",
    fontSize: 15,
    fontWeight: "bold"
  },
  textInput: {
    borderWidth: 1,
    borderColor: "rgb(224,224,224)",
    backgroundColor: "rgb(250,250,250)",
    paddingHorizontal: 10,
    paddingVertical: 15,
    fontSize: 15,
    borderRadius: 5,
    marginBottom: 20
  },
  textLink: { color: "rgb(107, 154, 201)", fontWeight: "bold" },
  footer: {
    flex: 1,
    borderTopWidth: 1,
    borderColor: "rgb(218,218,218)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  textFooter: { color: "rgb(183,183,183)", fontWeight: "bold" }
})
