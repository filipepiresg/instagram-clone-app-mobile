import React from "react"
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  StatusBar
} from "react-native"
import { RNCamera } from "react-native-camera"

const { width, height } = Dimensions.get("window")

export default class SendScreen extends React.Component {
  takePicture = async () => {
    if (this.camera) {
      const options = { quality: 0.5, base64: true }
      const data = await this.camera.takePictureAsync(options)

      alert(data.uri)
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} barStyle="default" />
        <RNCamera
          ref={camera => {
            this.camera = camera
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          autoFocus={RNCamera.Constants.AutoFocus.on}
          flashMode={RNCamera.Constants.FlashMode.off}
          permissionDialogTitle={"Permission to use camera"}
          permissionDialogMessage={
            "We need your permission to use your camera phone"
          }
        />
        <View style={styles.buttonContainer}>
          <TouchableOpacity onPress={this.takePicture} style={styles.capture}>
            {/* <Text style={styles.buttonText}> SNAP </Text> */}
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "black"
  },
  preview: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  buttonContainer: {
    flex: 0,
    // flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  capture: {
    flex: 0,
    backgroundColor: "#fff",
    borderRadius: height / 24,
    height: height / 12,
    width: height / 12,
    // padding: 15,
    // paddingHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
    // alignSelf: "center",
    margin: 20
  },
  buttonText: {
    fontSize: 14
  }
})
/*
const SendScreen = () => (
  <RNCamera
    ref={camera => {
      this.camera = camera
    }}
    style={styles.preview}
    type={RNCamera.Constants.Type.back}
    autoFocus={RNCamera.Constants.AutoFocus.on}
    flashMode={RNCamera.Constants.FlashMode.off}
    permissionDialogTitle={"Permission to use camera"}
    permissionDialogMessage={"We need your permission to use your camera phone"}
  />
)
 (
  <View
    style={{
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#000"
    }}
  >
    <Text style={{ color: "#fff", fontWeight: "bold" }}>Camera</Text>
  </View>
  ) 
  export default SendScreen
  */
