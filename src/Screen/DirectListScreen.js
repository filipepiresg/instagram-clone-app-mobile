import React, { Component } from "react"
import { Text, TouchableOpacity, FlatList, TextInput, View } from "react-native"
import Icon from "react-native-vector-icons/Ionicons"

export default class DirectListScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: () => (
      <Text style={{ fontSize: 16, fontWeight: "bold" }}>Direct</Text>
    ),
    headerRight: (
      <TouchableOpacity
        activeOpacity={0.5}
        style={{ marginRight: 20 }}
        //   onPress={() => navigation.navigate("DirectList")}
      >
        <Icon name="ios-add" size={40} color="#000" />
      </TouchableOpacity>
    ),
    headerLeft: (
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => navigation.pop()}
        style={{ marginLeft: 20 }}
      >
        <Icon name="ios-arrow-back" size={35} color="#000" />
      </TouchableOpacity>
    )
  })
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      search: "",
      error: null,
      isRefreshing: false
    }
    // this._handlerRefreshing()
  }
  /* 
  componentDidMount() {
    this._handlerRefreshing()
  }
  componentWillUnmount() {
    // this.
  }
   */
  _handlerRefreshing = () => {
    // const { isRefreshing } = this.state
    // this.setState({ isRefreshing: !isRefreshing })
    const data = [
      {
        image: "",
        username: "filipepiresg"
      }
    ]
    this.setState({ isRefreshing: true })

    setTimeout(() => {
      this.setState({ data, isRefreshing: false })
    }, 2000)
  }

  renderHeader = () => {
    const { search } = this.state
    return (
      <TextInput
        value={search}
        placeholder="Search"
        style={{
          flex: 1,
          padding: 10,
          borderRadius: 10,
          textAlign: "center",
          backgroundColor: "#eee"
        }}
        onChangeText={search => this.setState({ search })}
      />
    )
  }

  _render(item) {
    console.log(item)
    return (
      <View
        style={{
          height: 100,
          borderRadius: 10,
          marginVertical: 20,
          backgroundColor: "#eee",
          justifyContent: "center"
        }}
      >
        <Text style={{ textAlign: "center" }}>{item.username}</Text>
      </View>
    )
  }
  render() {
    const { data, isRefreshing } = this.state
    return (
      <FlatList
        data={data}
        renderItem={({ item }) => this._render(item)}
        refreshing={isRefreshing}
        keyExtractor={item => item.username}
        ListHeaderComponent={this.renderHeader}
        style={{ paddingHorizontal: 20 }}
        ListEmptyComponent={() => (
          <Text style={{ textAlign: "center" }}>List Empty</Text>
        )}
        // refreshControl
        onRefresh={this._handlerRefreshing}
      />
    )
  }
}
