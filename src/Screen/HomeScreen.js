import React, { Component } from "react"
import {
  Text,
  View,
  TouchableOpacity,
  AsyncStorage,
  FlatList
} from "react-native"
import Icon from "react-native-vector-icons/Ionicons"

import { Post } from "../Components"
import * as api from "../bin/api"

export default class HomeScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: () => (
      <Text
        style={{ fontFamily: "Billabong", fontSize: 35, fontWeight: "bold" }}
      >
        InstagramClone
      </Text>
    ),
    headerRight: (
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => navigation.navigate("DirectList")}
      >
        <Icon
          name="ios-send"
          size={35}
          color="#000"
          style={{ marginRight: 20 }}
        />
      </TouchableOpacity>
    )
  })
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      isRefreshing: false
    }
  }
  componentDidMount() {
    this._refreshData()
  }

  _refreshData = () => {
    this.setState({ isRefreshing: true })
    api
      .getPosts()
      .then(value => this.setState({ data: value.data }))
      .catch(err => console.log(err))
    this.setState({ isRefreshing: false })
  }

  _handleLogout = async () => {
    const { navigation } = this.props
    await AsyncStorage.removeItem("@instagram-clone:userToken", () =>
      navigation.navigate("AuthLoading")
    )
  }
  _keyExtractor = item => {
    return item.key.toString()
  }
  _renderItem = ({ item }) => {
    return <Post>{item}</Post>
  }
  render() {
    const { data, isRefreshing } = this.state
    return (
      <View>
        <FlatList
          data={data}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          ItemSeparatorComponent={() => <View style={{ marginBottom: 10 }} />}
          contentContainerStyle={{ paddingBottom: 10 }}
          refreshing={isRefreshing}
          onRefresh={this._refreshData}
          showsVerticalScrollIndicator={false}
        />
      </View>
    )
  }
}
