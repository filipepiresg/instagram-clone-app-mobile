import React, { Component } from "react"
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Alert,
  AsyncStorage,
  KeyboardAvoidingView
} from "react-native"
import Icon from "react-native-vector-icons/Ionicons"

import { createUser } from "../bin/authService"

export default class SignUpScreen extends Component {
  static navigationOptions = {
    header: null
  }
  state = {
    username: "",
    email: "",
    password: "",
    isActiveButton: false
  }

  componentDidUpdate(_, prevState) {
    if (
      prevState.username !== this.state.username ||
      prevState.email !== this.state.email ||
      prevState.password !== this.state.password
    ) {
      this._handleButton()
    }
  }

  _handleButton = () => {
    const { username, email, password } = this.state
    if (username && email && password) this.setState({ isActiveButton: true })
    else this.setState({ isActiveButton: false })
  }

  _signUpAsync = () => {
    const { username, email, password } = this.state
    const { navigation } = this.props

    // connect with back-end and returns the token to save on storage
    createUser(username, email, password)
      .then(data => {
        AsyncStorage.setItem(
          "@instagram-clone:userToken",
          JSON.stringify(data),
          () => navigation.navigate("Home")
        )
      })
      .catch(err => {
        console.error(err)
        // Alert.alert("Error", err)
      })
  }

  render() {
    const { username, email, password, isActiveButton } = this.state
    const { navigation } = this.props
    return (
      <KeyboardAvoidingView style={styles.container}>
        <View style={styles.containerLogo}>
          <TouchableOpacity onPress={() => navigation.pop()}>
            <Icon color="#fff" name="ios-close" size={60} />
          </TouchableOpacity>
          <Text style={styles.textLogo}>InstagramClone</Text>
          <Text style={styles.textLogoFooter}>
            Sign up to see photos and videos from your friends.
          </Text>
        </View>
        <View style={styles.containerData}>
          <TextInput
            placeholder="Email"
            value={email}
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.textInput}
            onChangeText={email => this.setState({ email })}
          />
          <TextInput
            placeholder="Username"
            value={username}
            keyboardType="name-phone-pad"
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.textInput}
            onChangeText={username => this.setState({ username })}
          />
          <TextInput
            placeholder="Password"
            value={password}
            keyboardType="default"
            autoCapitalize="none"
            autoCorrect={false}
            secureTextEntry
            onChangeText={password => this.setState({ password })}
            style={styles.textInput}
          />
          <TouchableOpacity
            disabled={!isActiveButton}
            onPress={this._signUpAsync}
          >
            {/* <Text style={styles.textLink}>Sign Up With Email</Text> */}
            <View
              style={[
                styles.buttonSignIn,
                {
                  backgroundColor: isActiveButton
                    ? "rgb(70,100, 240)"
                    : "rgb(170,200, 240)"
                }
              ]}
            >
              <Text style={styles.textButton}>Create User</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.containerFooter}>
          <View style={{ flex: 3 }} />
          <View style={styles.footer}>
            <Text style={styles.textFooter}>Already have an account?</Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("SignIn")}
            >
              <Text style={styles.textLink}> Sign In.</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: { flex: 1 },
  containerLogo: {
    flex: 2,
    backgroundColor: "rgb(255,87,51)",
    paddingHorizontal: 30,
    justifyContent: "center",
    alignItems: "center"
  },
  containerData: { flex: 2, paddingHorizontal: 15, justifyContent: "center" },
  containerFooter: { flex: 1 },
  textLogo: {
    marginTop: 10,
    fontFamily: "Billabong",
    fontSize: 60,
    fontWeight: "bold",
    color: "#fff"
  },
  textLogoFooter: {
    flexWrap: "wrap",
    textAlign: "center",
    fontSize: 18,
    fontWeight: "bold",
    color: "#fff"
  },
  textInput: {
    borderWidth: 1,
    borderColor: "rgb(224,224,224)",
    backgroundColor: "rgb(250,250,250)",
    paddingHorizontal: 10,
    paddingVertical: 15,
    fontSize: 15,
    borderRadius: 5,
    marginBottom: 20
  },
  textButton: {
    textAlign: "center",
    color: "#fff",
    fontWeight: "bold"
  },
  textLink: { color: "rgb(107, 154, 201)", fontWeight: "bold" },
  footer: {
    flex: 1,
    borderTopWidth: 1,
    borderColor: "rgb(218,218,218)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  textFooter: { color: "rgb(183,183,183)", fontWeight: "bold" },
  buttonSignIn: {
    borderRadius: 5,
    paddingVertical: 15
    // marginVertical: 30
  }
})
