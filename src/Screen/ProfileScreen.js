import React from "react"
import { View, Text, TouchableOpacity, AsyncStorage } from "react-native"
import Icon from "react-native-vector-icons/Ionicons"

export default class ProfileScreen extends React.Component {
  static navigationOptions = {
    headerTitle: () => (
      <Text style={{ fontSize: 16, fontWeight: "bold" }}>Profile</Text>
    ),
    headerRight: (
      <TouchableOpacity activeOpacity={0.5}>
        <Icon
          name="ios-menu"
          size={35}
          color="#000"
          style={{ marginRight: 20 }}
        />
      </TouchableOpacity>
    )
  }
  constructor(props) {
    super(props)
    this.state = {
      username: "",
      email: ""
    }
    this._handlerStorageAsync()
  }

  _handlerStorageAsync = async () => {
    await AsyncStorage.getItem("@instagram-clone:userToken", (err, data) => {
      if (err) console.error(err)
      else this.setState({ username: data.username, email: data.email })
    })
  }

  _handlerLogout = async () => {
    const { navigation } = this.props
    await AsyncStorage.removeItem("@instagram-clone:userToken", () =>
      navigation.navigate("AuthLoading")
    )
  }
  render() {
    return (
      <View style={{ flex: 1, paddingHorizontal: 10 }}>
        <Text> Profile Screen </Text>
        <TouchableOpacity onPress={this._handlerLogout}>
          <View
            style={{
              backgroundColor: "#ddd",
              borderRadius: 5,
              paddingVertical: 10
            }}
          >
            <Text style={{ textAlign: "center" }}>Logout</Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}
