import React, { Component } from "react"
import { Text, View, TextInput } from "react-native"

export default class DiscoverScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      search: ""
    }
  }
  static navigationOptions = {
    headerTitle: () => (
      <TextInput
        placeholder="search"
        // value={this.state.search}
        placeholderTextColor="#000"
        style={{
          backgroundColor: "#eee",
          flex: 1,
          borderRadius: 10,
          marginHorizontal: 10,
          padding: 10,
          textAlign: "center"
        }}
        onChangeText={search => this.setState({ search })}
      />
    )
  }
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Text> Discover </Text>
      </View>
    )
  }
}
