import React from "react"
import {
  Text,
  View,
  AsyncStorage,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
  KeyboardAvoidingView
} from "react-native"

import { verifyUser } from "../bin/authService"

export default class SignInScreen extends React.Component {
  static navigationOptions = { header: null }

  state = {
    username: "",
    password: "",
    isActiveButton: false
  }

  componentDidUpdate(_, prevState) {
    if (
      prevState.username !== this.state.username ||
      prevState.password !== this.state.password
    )
      this._handleActiveLoginButton()
  }

  _cleanStates = (callback = Function) => {
    this.setState(
      { username: "", password: "", isActiveButton: false },
      () => callback
    )
  }

  _signInAsync = () => {
    const { navigation } = this.props
    const { username, password } = this.state

    // connect with back-end and returns the token to save on storage
    verifyUser(username, password)
      .then(async data => {
        await AsyncStorage.setItem(
          "@instagram-clone:userToken",
          JSON.stringify(data),
          () => navigation.navigate("Home")
        )
      })
      .catch(err => {
        this._cleanStates(Alert.alert("Error", err))
      })
  }

  _handleActiveLoginButton = () => {
    const { username, password } = this.state
    if (username && password) this.setState({ isActiveButton: true })
    else this.setState({ isActiveButton: false })
  }

  render() {
    const { navigation } = this.props
    const { username, password, isActiveButton } = this.state
    return (
      <KeyboardAvoidingView style={styles.container}>
        <View style={styles.containerLogo}>
          <Text style={styles.textLogo}>InstagramClone</Text>
        </View>
        <View style={styles.containerData}>
          <TextInput
            placeholder="Username or e-mail"
            value={username}
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.textInput}
            onChangeText={username => this.setState({ username })}
          />
          <TextInput
            placeholder="Password"
            value={password}
            keyboardType="default"
            autoCapitalize="none"
            autoCorrect={false}
            secureTextEntry
            onChangeText={password => this.setState({ password })}
            style={styles.textInput}
          />
          <View style={{ alignItems: "flex-end" }}>
            <TouchableOpacity
              onPress={() => this._cleanStates(navigation.navigate("Forgot"))}
            >
              <Text style={styles.textLink}>Forgot Password?</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            disabled={!isActiveButton}
            onPress={this._signInAsync}
          >
            <View
              style={[
                styles.buttonLogin,
                {
                  backgroundColor: isActiveButton
                    ? "rgb(70,100, 240)"
                    : "rgb(170,200, 240)"
                }
              ]}
            >
              <Text style={styles.textButtonLogin}>Login</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.containerFooter}>
          <View style={{ flex: 3 }} />
          <View style={styles.footer}>
            <Text style={styles.textFooter}>Don't have an account?</Text>
            <TouchableOpacity
              onPress={() => this._cleanStates(navigation.navigate("SignUp"))}
            >
              <Text style={styles.textLink}> Sign Up.</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  containerLogo: { flex: 2, justifyContent: "center" },
  containerData: { flex: 2, paddingHorizontal: 15 },
  containerFooter: { flex: 1 },
  textLogo: {
    fontFamily: "Billabong",
    fontSize: 60,
    textAlign: "center",
    fontWeight: "bold"
  },
  textInput: {
    borderWidth: 1,
    borderColor: "rgb(224,224,224)",
    backgroundColor: "rgb(250,250,250)",
    paddingHorizontal: 10,
    paddingVertical: 15,
    fontSize: 15,
    borderRadius: 5,
    marginBottom: 20
  },
  buttonLogin: {
    borderRadius: 5,
    paddingVertical: 15,
    marginVertical: 30
  },
  textButtonLogin: {
    textAlign: "center",
    color: "white",
    fontSize: 15,
    fontWeight: "bold"
  },
  textLink: { color: "rgb(107, 154, 201)", fontWeight: "bold" },
  footer: {
    flex: 1,
    borderTopWidth: 1,
    borderColor: "rgb(218,218,218)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  textFooter: { color: "rgb(183,183,183)", fontWeight: "bold" }
})
