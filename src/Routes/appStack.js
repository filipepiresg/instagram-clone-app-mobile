import React from "react"
import { View, Text } from "react-native"
import {
  createStackNavigator,
  createBottomTabNavigator
} from "react-navigation"
import Icon from "react-native-vector-icons/FontAwesome5"

import {
  HomeScreen,
  DiscoverScreen,
  ProfileScreen,
  NotificationScreen,
  DirectListScreen,
  SendScreen
} from "../Screen"

handleIcons = (name, focused, tintColor) => (
  <Icon name={name} color={focused ? "#000" : tintColor} size={28} />
)

const HomeStack = createStackNavigator({
  Home1: HomeScreen,
  DirectList: DirectListScreen
})

const DiscoverStack = createStackNavigator({
  Discover1: DiscoverScreen
})

const NotificationStack = createStackNavigator({
  Notification1: NotificationScreen
})

const ProfileStack = createStackNavigator({
  Profile1: ProfileScreen
})

export default createBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) =>
          handleIcons("home", focused, tintColor)
      }
    },
    Discover: {
      screen: DiscoverStack,
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) =>
          handleIcons("search", focused, tintColor)
      }
    },
    Send: {
      // screen: HomeScreen,
      screen: SendScreen,
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) =>
          handleIcons("plus-square", focused, tintColor)
      }
    },
    Notification: {
      screen: NotificationStack,
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) =>
          handleIcons("heart", focused, tintColor)
      }
    },
    Profile: {
      screen: ProfileStack,
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) =>
          handleIcons("user", focused, tintColor)
      }
    }
  },
  {
    initialRouteName: "Home",
    tabBarOptions: {
      showLabel: false,
      tabStyle: {
        justifyContent: "flex-end"
      }
    }
  }
)
