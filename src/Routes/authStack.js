import { createStackNavigator } from "react-navigation"
import { SignInScreen, SignUpScreen, ForgotPassScreen } from "../Screen"
export default createStackNavigator(
  {
    SignIn: SignInScreen,
    SignUp: SignUpScreen,
    Forgot: ForgotPassScreen
  },
  {
    initialRouteName: "SignIn"
  }
)
