import { createSwitchNavigator, createAppContainer } from "react-navigation"

import { AuthLoadingScreen } from "../Screen"
import AuthStack from "./authStack"
import AppStack from "./appStack"

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: AppStack,
      Auth: AuthStack
    },
    {
      initialRouteName: "AuthLoading"
    }
  )
)
