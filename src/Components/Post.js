import React, { Component } from "react"
import {
  Text,
  StyleSheet,
  View,
  Dimensions,
  TextInput,
  TouchableOpacity,
  Image
} from "react-native"
import Icon from "react-native-vector-icons/Ionicons"

const { height } = Dimensions.get("window")

export default class Post extends Component {
  state = {
    isLike: false,
    comment: "",
    likes: 0
  }
  componentDidMount() {
    this.setState({ likes: this.props.children.likes || 0 })
  }

  _handlerButtonLike = () => {
    const { isLike, likes } = this.state
    this.setState(prevState => {
      if (prevState.isLike)
        return { isLike: !prevState.isLike, likes: likes - 1 }
      return { isLike: !prevState.isLike, likes: likes + 1 }
    })
  }
  render() {
    const { likes, isLike, comment } = this.state
    const { username, location, photoURL } = this.props.children
    return (
      <View style={styles.container}>
        <View style={styles.containerHeader}>
          <TouchableOpacity style={styles.buttonProfile}>
            <Icon name="ios-person" size={25} />
            <View style={{ marginLeft: 10 }}>
              <Text style={{ fontSize: 16, fontWeight: "600" }}>
                {username}
              </Text>
              <Text style={{ fontSize: 12 }}>{location}</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.containerMedia}>
          <Image
            source={{ uri: photoURL }}
            resizeMode="contain"
            style={{ flex: 1 }}
          />
        </View>
        <View style={styles.containerFooter}>
          <Text>{likes} persons liked this</Text>
          <View style={styles.containerComment}>
            <TextInput
              style={styles.textInputComment}
              value={comment}
              onChangeText={comment => this.setState({ comment })}
              placeholder="comment this..."
            />
            <TouchableOpacity onPress={this._handlerButtonLike}>
              {isLike ? (
                <Icon name="ios-heart" size={25} color="red" />
              ) : (
                <Icon name="ios-heart" size={25} />
              )}
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: height * 0.4,
    backgroundColor: "#eee"
  },
  containerHeader: {
    flex: 1,
    padding: 10,
    justifyContent: "center"
  },
  containerMedia: { flex: 3.8, backgroundColor: "rgba(0,0,0,0.6)" },
  containerFooter: { flex: 1.2, padding: 10, justifyContent: "center" },
  buttonProfile: { flexDirection: "row", alignItems: "center" },
  textInputComment: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 10,
    marginRight: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#ccc"
  },
  containerComment: { flexDirection: "row", alignItems: "center" }
})
