import * as api from "./api"

export const verifyUser = (username, password) => {
  return new Promise((resolve, reject) => {
    api
      .getLogin(username, password)
      .then(value => resolve(value.data))
      .catch(err => reject(err.message))
  })
}

export const createUser = (username, password, email) => {
  return new Promise((resolve, reject) => {
    api
      .createUser(username, password, email)
      .then(value => resolve(value.data))
      .catch(err => reject(err.message))
  })
}
export const recoverPass = (username, email) => {
  return new Promise((resolve, reject) => {
    api
      .recoverPassword(username, email)
      .then(value => resolve(value.data))
      .catch(err => reject(err.message))
  })
}
