import axios from "axios"
import Unsplash from "unsplash-js/native"

axios.create({
  baseURL: "localhost:3000"
})

import db from "../config/db.json"
import users from "../config/users.json"
import { toJson } from "unsplash-js/src/unsplash"

const ACCESS_TOKEN_UNSPLASH =
  "f4a5cf08a2fe91d5655a65c826136dbf5e6b8a86e4073e54888e10e8df6df2bc"
const SECRET_KEY_UNSPLASH =
  "7ac458db4f6bd6cd439880948148542a0ad0f9485ce331828a819c21905b228f"
const AUTORIZATION_CODE_UNSPLASH =
  "28a126f691ad15e82574d0b4882ac3a541b2dd24fd76a24466d34ad30ec4baf8"

const unsplash = new Unsplash({
  applicationId: ACCESS_TOKEN_UNSPLASH,
  secret: SECRET_KEY_UNSPLASH,
  callbackUrl: "{CALLBACK_URL}"
})

const TIMEOUT = 800

const verifyDB = () => {}

export const getLogin = (username, password) => {
  // usingOAuth2.0
  // receive { acess_token, user:{ id, username, full_name, profile_picture } } from API Instagram
  // to save as a token in storage
  return new Promise(async (resolve, reject) => {
    await setTimeout(() => {
      if (!users) return reject({ code: 500, message: "Error Internal" })
      users.forEach(user => {
        if (user.username === username) {
          if (user.password === password) {
            return resolve({ code: 200, data: JSON.stringify(user) })
          } else {
            return reject({ code: 401, message: "Password is invalid" })
          }
        }
      })
      return reject({ code: 404, message: "User Not Found" })
    }, TIMEOUT)
  })
}

export const createUser = (username, password, email) => {
  return new Promise(async (resolve, reject) => {
    await setTimeout(() => {
      if (!users) return reject({ code: 500, message: "Error Internal" })
      users.forEach(user => {
        if (username === user.username || email === user.email) {
          return reject({ code: 403, message: "User Already Registered" })
        }
      })
      // users.push(newUser)
      return resolve({ code: 201, data: { username, email } })
    }, TIMEOUT)
  })
}

export const recoverPassword = (username, email) => {
  return new Promise(async (resolve, reject) => {
    setTimeout(() => {
      if (!users) return reject({ code: 500, message: "Error Internal" })
      users.forEach(user => {
        if (user.email === email)
          if (user.username === username) {
            return resolve({ code: 200, data: { password: user.password } })
          } else {
            return reject({ code: 401, message: "Values don't matched" })
          }
      })
      return reject({ code: 404, message: "User not found" })
    }, TIMEOUT)
  })
}

export const getPosts = () => {
  return new Promise(async (resolve, reject) => {
    const data = []
    for (let i = 0; i < 50; i++) {
      let randomUser = {}
      await fetch("https://randomuser.me/api/", {
        method: "GET"
      })
        .then(({ _bodyText }) => {
          const { results } = JSON.parse(_bodyText)
          const {
            login: { username, uuid },
            location: { city },
            picture: { large },
            dob: { age }
          } = results[0]
          randomUser = {
            key: uuid,
            username,
            location: city,
            photoURL: large,
            likes: age
          }
          // console.log(randomUser)
          data.push(randomUser)
          return resolve({ code: 200, data })
        })
        .catch(err => reject({ code: 400, message: err }))
    }
  })
}
